# DCFeed Framework 0.9
DCFeed is an Objective-C framework for downloading and parsing RSS (1.* and 2.*) and Atom web feeds. It is a very simple and clean implementation that reads the following information from a web feed


![create-custom-ios-framework.png](https://bitbucket.org/repo/bo4yRn/images/2347880035-create-custom-ios-framework.png)
#How to use
import DCFeed.framework  into your project
![screenshot](http://diegocaridei.altervista.org/blog/wp-content/uploads/2013/10/import.png)

import the framework into the class
--#import <DCFeed/DCFeed.h>

  @property(nonatomic,retain)DCFeed *feed;
  @property(nonatomic,retain)NSString *path;


- (void)viewDidLoad
{
    [super viewDidLoad];
    _path=@"http://rss.nytimes.com/services/xml/rss/nyt/InternationalHome.xml";
    _feed=[[DCFeed alloc]init];
    [_feed parseXMLFileAtURL:_path];
    [self.tableView reloadData];

}


NSLog(@"%@",cell.testo.text=[[_feed.listFeed objectAtIndex:indexPath.row] objectForKey:@"title"]);

 
#Feed Information

*title

*link

*description

*pubDate