//
//  DCFeed.h
//  DCFeed
//
//  Created by Diego Caridei on 01/11/13.
//  Copyright (c) 2013 Diego Caridei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DCFeed : NSObject<NSXMLParserDelegate>
// parser XML
@property(nonatomic,retain) NSXMLParser *rssParser;
// elenco degli elementi letti dal feed
@property(nonatomic,retain) NSMutableArray *listFeed;
//variabile temporanea pe ogni elemento
@property(nonatomic,retain) NSMutableDictionary *item;
// valori dei campi letti dal feed
@property(nonatomic,retain) NSString *currentElement;
@property(nonatomic,retain) NSMutableString *currentTitle, *currentDate, *currentSummary, *currentLink;
- (void)parseXMLFileAtURL:(NSString *)URL;

@end
