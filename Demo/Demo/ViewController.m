//
//  ViewController.m
//  Demo
//
//  Created by Diego Caridei on 01/11/13.
//  Copyright (c) 2013 Diego Caridei. All rights reserved.
//

#import "ViewController.h"
#import "MiaCella.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _path=@"http://rss.nytimes.com/services/xml/rss/nyt/InternationalHome.xml";
    _feed=[[DCFeed alloc]init];
    [_feed parseXMLFileAtURL:_path];
    [self.tableView reloadData];

}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //numero di righe nella tabella
    return [_feed.listFeed count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CustomCell";
    
    MiaCella *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MiaCella alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    [[cell textLabel] setTextColor:[UIColor blackColor]];
    
    cell.testo.text=[[_feed.listFeed objectAtIndex:indexPath.row] objectForKey:@"title"];
    NSLog(@"%@",cell.testo.text=[[_feed.listFeed objectAtIndex:indexPath.row] objectForKey:@"title"]);
    
    cell.detailTextLabel.text=[[_feed.listFeed  objectAtIndex:indexPath.row] objectForKey:@"pubDate"];
    return cell;
    
}

@end
