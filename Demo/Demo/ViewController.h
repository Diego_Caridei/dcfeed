//
//  ViewController.h
//  Demo
//
//  Created by Diego Caridei on 01/11/13.
//  Copyright (c) 2013 Diego Caridei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DCFeed/DCFeed.h>

@interface ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,retain)DCFeed *feed;
@property(nonatomic,retain)NSString *path;

@end
