//
//  AppDelegate.h
//  Demo
//
//  Created by Diego Caridei on 01/11/13.
//  Copyright (c) 2013 Diego Caridei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
